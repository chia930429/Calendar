//
//  ViewController.swift
//  Calendar
//
//  Created by Brenda on 2022/5/4.
//

import UIKit
import FSCalendar

class ViewController: UIViewController {

    var collecitonView: UICollectionView!
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "授課時間"
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        return label
    }()
    
    private let nextButton: UIButton = {
        let button = UIButton()
        button.tintColor = .systemGray3
        button.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.calender_gray.cgColor
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.widthAnchor.constraint(equalToConstant: 38).isActive = true
        return button
    }()
    private let prevButton: UIButton = {
        let button = UIButton()
        button.tintColor = .systemGray3
        button.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.calender_gray.cgColor
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.widthAnchor.constraint(equalToConstant: 38).isActive = true
        return button
    }()
    
    private let showTimeLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18)
        label.text = "2022/05/22 - 2022/05/28"
        return label
    }()
    
    private let noteLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 13, weight: .semibold)
        label.text = "*時間以 台北（GMT+8:00）顯示"
        label.textAlignment = .right
        return label
    }()
    
    private var viewModel = ViewModel()
    private let width = UIApplication.shared.delegate?.window
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialView()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        collecitonView.collectionViewLayout.invalidateLayout()
    }
    
    private func setUpInitialView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        
        let itemSizeWidth = (UIScreen.main.bounds.width - 10) / 8
        layout.itemSize = CGSize(width: itemSizeWidth, height: UIScreen.main.bounds.height - 100)
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 3
        layout.estimatedItemSize = .zero
        
        collecitonView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collecitonView.register(CollectionViewCell.self, forCellWithReuseIdentifier: "item")
        collecitonView.translatesAutoresizingMaskIntoConstraints = false
        collecitonView.delegate = self
        collecitonView.dataSource = self
//        collecitonView.isScrollEnabled = false
        
        let topLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        topLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        topLayout.itemSize = CGSize(width: itemSizeWidth, height: 50)
        topLayout.minimumLineSpacing = 1
        topLayout.minimumInteritemSpacing = 3
        topLayout.estimatedItemSize = .zero
        let calendar = FSCalendar()
        calendar.dataSource = self
        calendar.delegate = self
        calendar.scope = .week
        calendar.translatesAutoresizingMaskIntoConstraints = false
        
        let buttonStack = UIStackView(arrangedSubviews: [prevButton, nextButton])
        
        let midStack = UIStackView(arrangedSubviews: [buttonStack, showTimeLabel])
        midStack.distribution = .fillProportionally
        midStack.spacing = 20
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, midStack, noteLabel])
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        view.addSubview(calendar)
        calendar.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 10).isActive = true
        calendar.heightAnchor.constraint(equalToConstant: 200).isActive = true
        calendar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        calendar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        view.addSubview(collecitonView)
        collecitonView.topAnchor.constraint(equalTo: calendar.bottomAnchor, constant: -120).isActive = true
        collecitonView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        collecitonView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        collecitonView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! CollectionViewCell
        cell.configureCollectionCell(viewModel: viewModel, index: indexPath.row)
        
        DispatchQueue.main.async {
            cell.tableView.reloadData()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = collectionView.bounds.width / 8
        return CGSize(width: itemSize, height: UIScreen.main.bounds.height - 90 - 200)
    }
}


extension ViewController: FSCalendarDelegate, FSCalendarDataSource {
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        //disabled scroll to past time
        return Date()
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        //disable selected past time-->?
        return !(date .compare(Date()) == .orderedAscending)
        
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        //TODO: date change -- data change
        
        if calendar.currentPage.compare(.now).rawValue == 1 {
            self.viewModel.data = nil
            
        } else {
            self.viewModel.fetchData { result, error in
                self.viewModel.data = result
            }
        }
        DispatchQueue.main.async {
            self.collecitonView.reloadData()
        }
    }
    
}
