//
//  CollectionViewCell.swift
//  Calendar
//
//  Created by Brenda Chang on 2022/5/4.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    //ToDo: add data
    enum weekDayToIndex: Int {
        case sun = 0
        case mon, tue, wed, thur, fir, sat
        
    }
    var viewModel: ViewModel!
    var indexOfCollection: Int?
    
    var tableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.register(TableViewCell.self, forCellReuseIdentifier: "cell")
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }
    
    private func setUpLayout() {
        
        //for table
        addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder: ) has not been implemented")
    }
    
    
    func configureCollectionCell(viewModel: ViewModel, index: Int) {
        self.viewModel = viewModel
        self.indexOfCollection = index
  
    }
    
}


extension CollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let index = indexOfCollection {
            
            let count = (self.viewModel?.data?.time[index].timeArray.count ?? 0)
            return count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TableViewCell, let index = indexOfCollection, let model =  self.viewModel.data?.time[index].timeArray[indexPath.row] else { return UITableViewCell() }
        print(model, "the model")
        cell.configureCell(model: model, index: indexPath.row)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
