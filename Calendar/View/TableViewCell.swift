//
//  TableViewCell.swift
//  Calendar
//
//  Created by Brenda Chang on 2022/5/5.
//

import UIKit

class TableViewCell: UITableViewCell {

    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 13, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(timeLabel)
        timeLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        timeLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        timeLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: TimeArray?, index: Int) {
        timeLabel.text = model?.start
        timeLabel.textColor = model?.available == true ? .calender_green : .calender_gray
    }
}
