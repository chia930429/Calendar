//
//  ViewModel.swift
//  Calendar
//
//  Created by Brenda Chang on 2022/5/4.
//
import Foundation
import Combine



class ViewModel {
    
    var data: Result?
   
    init() {
        fetchData { result, error in
            guard error == nil else {
                return
            }
            self.data = result
        }
        declarative()
    }
    
    func declarative() {
        let subscription = URLSession.shared.dataTaskPublisher(for: URL(fileURLWithPath: "mockfile"))
            .map{ $0.data }
            .sink { completion in
                print(completion)
            } receiveValue: { data in
                print("data see", data)
            }

        
    }
    func fetchData(completionHandler: @escaping(Result?, FetchError?) -> Void) {
        
        guard let url = Bundle.main.url(forResource: "mockfile", withExtension: "json") else {
            completionHandler(nil, FetchError.cannotAccessFile)
            return
        }
        do {
            
           
            let data = try Data(contentsOf: url)
            let responseData = try JSONDecoder().decode(Tutor.self, from: data)
            
            var Dict = [Date: CustomTime]()
            let item = responseData.available
            
            responseData.available.forEach { time in
                
                guard let endTime = time.end.toDate(), let startTime = time.start.toDate() else { return }
                var comparedRes = Calendar.current.compare(startTime, to: endTime.advanced(by: -1), toGranularity: .day)
                while comparedRes != .orderedDescending { //result
                    let startOfDay = Calendar.current.startOfDay(for: startTime)
                    let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: startTime)!
                    
                    let startOfNextDay = Calendar.current.startOfDay(for: nextDay)
                    let dateInterval: DateInterval
                    if comparedRes == .orderedSame { //same day
                        dateInterval = DateInterval(start: startTime, end: endTime)
                    } else {
                        dateInterval = DateInterval(start: startTime, end: startOfNextDay)
                    }
                    
                    
                    if Dict[startOfDay] == nil {
//                        Dict[startOfDay] = IntervalTime(start: <#T##String#>, end: <#T##String#>)
                    }
                    
                    
                }
            }
            
            
            
            
            
            
            
            completionHandler(nil, nil)
            //MARK: Json Format to Custom Format
            /*var result: Result?
            let data = try Data(contentsOf: url)
            let responseData = try JSONDecoder().decode(Tutor.self, from: data)
            var array: [Time] = []
            
            responseData.available.map { time in
                
                guard let startTime = time.start.toDate(), let endTime = time.end.toDate() else { return }
                var itemArray = [TimeArray]()
                timeIntervalCalculate(startTime: startTime, endTime: endTime).map {
                    
                    let stringTime = "\($0)"
                    let item = TimeArray(start: stringTime, available: true)
                    itemArray.append(item)
                }
                print("date", startTime, "converted\(startTime.getUnitDay())")
                let date = Time(date: "\(startTime.getUnitDay())", weekDay: startTime.getWeekDay(), timeArray: itemArray)
                array.append(date)
            }
            
            responseData.booked.map { time in
                guard let startTime = time.start.toDate(), let endTime = time.end.toDate() else { return }
                var itemArray = [TimeArray]()
                
                timeIntervalCalculate(startTime: startTime, endTime: endTime).forEach {
                    let stringTime = "\($0)"
                    let item = TimeArray(start: stringTime, available: false)
                    itemArray.append(item)
                }
                print("booked", startTime, "converted\(startTime.getUnitDay())")
                let date = Time(date: "\(startTime.getUnitDay())", weekDay: startTime.getWeekDay(), timeArray: itemArray)
                array.append(date)
            }
            
           
            result = Result(time: array.sorted(by: { lhs, rhs in lhs.date > rhs.date }))
            print("get array data", array)
            completionHandler(result, nil)*/

        } catch {
            completionHandler(nil, FetchError.parsingError)
        }
    }
    
    
    func setUpTimeDict(times: [CustomTime], state: Bool) {//state true is available
        times.forEach { time in
            
            guard let endTime = time.end.toDate(), let startTime = time.start.toDate() else { return }
            var comparedRes = Calendar.current.compare(startTime, to: endTime.advanced(by: -1), toGranularity: .day)
            while comparedRes != .orderedDescending { //result
                let startOfDay = Calendar.current.startOfDay(for: startTime)
                let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: startTime)!
                
                let startOfNextDay = Calendar.current.startOfDay(for: nextDay)
                let dateInterval: DateInterval
                if comparedRes == .orderedSame { //same day
                    dateInterval = DateInterval(start: startTime, end: endTime)
                } else {
                    dateInterval = DateInterval(start: startTime, end: startOfNextDay)
                }
                
                
                if state == true {
                    
                }
                
                
            }
        }
    }
    
    //MARK: Get all the time between startTime, endTime(30min/per)
    func timeIntervalCalculate(startTime: Date, endTime: Date) -> [String] {
        
        var copyStartTime = startTime
        
        var result: [String] = []
        
        result.append("\(startTime.formatted(date: .omitted, time: .shortened))")
        
        while(copyStartTime<endTime) {
            
            copyStartTime = copyStartTime.addingTimeInterval(30 * 60)
            
            result.append("\(copyStartTime.formatted(date: .omitted, time: .shortened))")
        }
        return result
    }
}


enum FetchError: Error, CustomDebugStringConvertible {
    
    case cannotAccessFile
    case parsingError
    case failedRequest(description: String)
    
    var debugDescription: String {
        switch self {
        case .cannotAccessFile:
            return "cannot get file throught. please check the file name, and correct format"
        case .parsingError:
            return "cannot parse data."
        case .failedRequest(description: let description):
            return description
        }
    }
}
