//
//  File.swift
//  Calendar
//
//  Created by Brenda Chang on 2022/5/4.
//

import Foundation
 
//Decode Format to grouped Data
struct IntervalTime {
    var availableInterval: [DateInterval]
    var bookedInterval: [DateInterval]
}

//MARK: -Custom Format to show Data
struct Result {
    let time: [Time]
}

struct Time {
    let date: String
    let weekDay: Int
    let timeArray: [TimeArray]
}

struct TimeArray {
    let start: String
    let available: Bool
}



//MARK: -Json Format
struct Tutor: Decodable {
    let available: [CustomTime]
    let booked: [CustomTime]
}

struct CustomTime: Decodable {
    let start: String
    let end: String
}



extension Date {
    
    //TODO : //(timeZone: String)
    func getToday() -> Int {
        let date = Date()
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        date. = TimeZone(identifier: TimeZone.current.identifier)
//        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
//        let today = formatter.string(from: date)
        
        return date.getUnitDay()
    }
    
    
    
    func getWeekDay() -> Int {
        return Calendar.current.component(.weekday, from: self)
    }
    
    func getUnitDay() -> Int {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "zh_Hant_TW")
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        return calendar.component(.day, from: self)
    }
}


extension String {
    func getWeekDay() -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
       
        guard let date = dateFormat.date(from: self) else { return "" }
        let interval = Int(date.timeIntervalSince1970)
        let days = Int(interval/86400) //   /24*60*60
        let weekday = ((days + 4)%7+7)%7
        weekday == 0 ? 7 : weekday
        return "(\(convertedWeek(week: weekday)))"
    }
    
    func convertedWeek(week: Int) -> String {
        switch week {
        case 1:
            return "一"
        case 2:
            return "二"
        case 3:
            return "三"
        case 4:
            return "四"
        case 5:
            return "五"
        case 6:
            return "六"
        case 0,7:
            return "日"
        default:
            return "  "
        }
    }
    func toDate() -> Date? {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: self)
    }
}
