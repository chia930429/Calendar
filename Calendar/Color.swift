//
//  Color.swift
//  Calendar
//
//  Created by Brenda Chang on 2022/5/4.
//

import Foundation
import UIKit

extension UIColor {
    
    static let calender_green = UIColor(red: 11/255, green: 201/255, blue: 188/255, alpha: 1)
    //#0BC9BC
    
    static let calender_gray = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1)
    //#D2D2D2
    
}
